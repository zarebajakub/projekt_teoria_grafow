# Projekt Teoria Grafow

W pliku Jakub_Zareba.pdf znajdują się część analityczna oraz opisowa część programistyczna.

## Jak uruchomić część programistyczną

krok 1. Trzeba utworzyć projekt w c++ oraz podmienić plik main.cpp z tym znajdującym się w repozytorium.

krok 2. Trzeba utworzyć (lub skopiować) plik tekstowy "graf_macierz.txt" i umieścić go **w tym samym folderze co znajduje się main.cpp**

krok 3. Zgodnie z podaną instrukcją umieścić w pliku tekstowym macierz incydencji:
- pierwsza 3 liczby to kolejno liczba wierzchołków, source i sink (numerowanie wierzchołków jest od 0)
- w kolejnych n liniach umieszczać po n liczb rozdzielonych spacją, gdzie n to liczba wierzchołków (czyli po prostu macierz incydencji)
- po uruchomieniu programu program wypisze, jaką macierz otrzymał. Można w ten sposób sprawdzić, czy dane zostały wprowadzone poprawnie
- przykładowa macierz znajduje się w pliku graf_macierz.txt
