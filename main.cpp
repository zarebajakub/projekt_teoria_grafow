#include <iostream>
#include <limits.h>
#include <queue>
#include <string.h>
#include <fstream>

using namespace std;

int V;

bool bfs(int rGraph[100][100], int s, int t, int parent[])
{
    bool visited[V];
    memset(visited, 0, sizeof(visited));

    queue<int> q;
    q.push(s);
    visited[s] = true;
    parent[s] = -1;

    while (!q.empty()) {
        int u = q.front();
        q.pop();

        for (int v = 0; v < V; v++) {
            if (visited[v] == false && rGraph[u][v] > 0) {
                if (v == t) {
                    parent[v] = u;
                    return true;
                }
                q.push(v);
                parent[v] = u;
                visited[v] = true;
            }
        }
    }

    return false;
}

int fordFulkerson(int graph[100][100], int s, int t)
{
    int u, v;

    int rGraph[100][100];
    for (u = 0; u < V; u++)
        for (v = 0; v < V; v++)
            rGraph[u][v] = graph[u][v];

    int parent[100];

    int max_flow = 0;

    while (bfs(rGraph, s, t, parent)){
        int path_flow = INT_MAX;
        for (v = t; v != s; v = parent[v]){
            u = parent[v];
            path_flow = min(path_flow, rGraph[u][v]);
        }
        for (v = t; v != s; v = parent[v]) {
            u = parent[v];
            rGraph[u][v] -= path_flow;
            rGraph[v][u] += path_flow;
        }
        max_flow += path_flow;
    }

    return max_flow;
}

int main()
{
    fstream plik;
    plik.open("graf_macierz.txt", ios::in);
    if (plik.good() == true){

    } else cout << "brak dostepu do pliku" << endl;

    int s,t;
    plik>>V>>s>>t;
    int graph[100][100];

    for (int i=0; i<V; i++){
        for (int j=0; j<V; j++){
            plik>>graph[i][j];
        }
    }
    plik.close();

    cout << "oto wprowadzone dane:" << endl;
    for (int i=0; i<V; i++){
        for (int j=0; j<V; j++){
            cout<<graph[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;

    cout << "Maksymalny przeplyw wynosi " << fordFulkerson(graph, s, t) << endl<<endl;

    return 0;
}
